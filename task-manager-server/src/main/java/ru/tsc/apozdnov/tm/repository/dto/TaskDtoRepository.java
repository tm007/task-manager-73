package ru.tsc.apozdnov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.apozdnov.tm.dto.model.TaskDtoModel;

import java.util.List;

@Repository
public interface TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDtoModel> {
    @NotNull
    List<TaskDtoModel> findAllByUserId(@NotNull String userId);

    @Nullable
    TaskDtoModel findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    @NotNull List<TaskDtoModel> findAllByProjectIdAndUserId(@NotNull String userId, @NotNull String projectId);

    void deleteAllByUserId(@NotNull String userId);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

}