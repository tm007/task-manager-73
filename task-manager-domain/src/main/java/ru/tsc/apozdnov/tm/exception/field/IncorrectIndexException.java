package ru.tsc.apozdnov.tm.exception.field;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public final class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException() {
        super("Error! Incorrect index...");
    }

}