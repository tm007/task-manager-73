package ru.tsc.apozdnov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.apozdnov.tm.dto.model.TaskDtoModel;
import ru.tsc.apozdnov.tm.dto.request.TaskListByProjectIdRequest;
import ru.tsc.apozdnov.tm.dto.response.TaskListByProjectIdResponse;
import ru.tsc.apozdnov.tm.event.ConsoleEvent;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskShowByProjectIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-show-by-project-id";

    @NotNull
    public static final String DESCRIPTION = "Show task list by project id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskShowByProjectIdListener.getName() == #event.name")
    public void executeEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(getToken(), projectId);
        @NotNull final TaskListByProjectIdResponse response = getTaskEndpoint().listTaskByProjectId(request);
        @Nullable final List<TaskDtoModel> tasks = response.getTasks();
        renderTasks(tasks);
    }

}