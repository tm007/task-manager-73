package ru.tsc.apozdnov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.apozdnov.tm.dto.model.UserDtoModel;
import ru.tsc.apozdnov.tm.dto.request.UserProfileRequest;
import ru.tsc.apozdnov.tm.dto.response.UserProfileResponse;
import ru.tsc.apozdnov.tm.enumerated.Role;
import ru.tsc.apozdnov.tm.event.ConsoleEvent;
import ru.tsc.apozdnov.tm.exception.entity.UserNotFoundException;

@Component
public final class UserShowProfileListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-show-profile";

    @NotNull
    public static final String DESCRIPTION = "Show user info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userShowProfileListener.getName() == #event.name")
    public void executeEvent(@NotNull final ConsoleEvent event) {
        @NotNull UserProfileResponse response = getUserEndpoint().showProfileUser(new UserProfileRequest(getToken()));
        @Nullable final UserDtoModel user = response.getUser();
        if (user == null) throw new UserNotFoundException();
        showUser(user);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}